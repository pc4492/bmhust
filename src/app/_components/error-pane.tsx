import { Card, Typography } from 'antd';

interface Props {
  error?: any;
}

export default function ErrorPane({ error }: Props) {
  return (
    <Card className="flex items-center justify-center h-screen text-center">
      <Typography.Title level={1}>Đã có lỗi xảy ra</Typography.Title>
      <Typography.Title level={3}>{error.status || error.message}</Typography.Title>
    </Card>
  );
}
