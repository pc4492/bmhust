'use client';

import { App, Avatar, Button, Card, Empty, Image, Tooltip } from 'antd';
import React, { useEffect } from 'react';
import { useState } from 'react';
import NextImage from 'next/image';
import { UserOutlined } from '@ant-design/icons';
type Props = {
  items: {
    id: string;
    name: string;
    age: number | string;
    images: {
      id: string;
      url: string;
    }[];
    address: string;
    hometown: string;
    hobbies: string;
    major: string;
  }[];
  hideControlBar?: boolean;
};

const CardItem = (props: Props) => {
  const { notification } = App.useApp();
  const [currentItemIndex, setCurrentItemIndex] = useState(0);
  const [currentImageIndex, setCurrentImageIndex] = useState(0);

  const currentItem = props.items[currentItemIndex];
  const totalImages = currentItem?.images.length;

  // Reset currentItemIndex when items update from parent component
  useEffect(() => {
    setCurrentItemIndex(0);
  }, [props.items]);

  const nextItem = () => {
    setCurrentImageIndex(0); // Reset image index when changing item
    setCurrentItemIndex((prevIndex) => (prevIndex + 1) % props.items.length);
  };

  const nextImage = () => {
    setCurrentImageIndex(
      (prevIndex) => (prevIndex + 1) % currentItem?.images?.length
    );
  };

  const prevImage = () => {
    setCurrentImageIndex((prevIndex) =>
      prevIndex === 0 ? currentItem?.images?.length - 1 : prevIndex - 1
    );
  };

  const handleRequestRelationship = async (
    isCrush: boolean,
    receiverId: string
  ) => {
    const response = await fetch(
      `/api/relationships/request`,
      {
        method: 'POST',
        body: JSON.stringify({
          isCrush,
          receiverId,
        }),
      }
    );

    switch (response.status) {
      case 201:
        notification.success({ message: 'Gửi yêu cầu thành  công!' });
        break;
      case 409:
        notification.error({ message: 'Đã gửi yêu cầu rồi' });
        break;
      default:
        notification.error({ message: 'Đã phát sinh lỗi' });
        break;
    }
  };

  if (!currentItem) {
    return (
      <Card bordered={false}>
        <Empty />
      </Card>
    );
  }

  return (
    <Card bordered={false}>
      <div className="flex justify-center items-center mt-10 mb-4">
        <Button
          style={{ border: 'none', width: '80px', height: '80px' }}
          disabled={currentImageIndex === 0}
          onClick={prevImage}
          icon={<Image preview={false} src="/left-arr.png" alt="left icon" />}
        />
        <div className='w-[400px] h-[400px] flex items-center justify-center relative'>
          {currentItem?.images[currentImageIndex]?.url ? (
            <NextImage
              src={currentItem?.images[currentImageIndex]?.url}
              alt="avatar"
              sizes="400px"
              fill
              style={{
                objectFit: 'contain',
              }}
            />
          ) : (
            <Avatar size={300} icon={<UserOutlined />} />
          )}
        </div>
        <Button
          style={{ border: 'none', width: '80px', height: '80px' }}
          disabled={currentImageIndex === totalImages - 1 || totalImages === 0}
          onClick={nextImage}
          icon={<Image preview={false} src="/right-arr.png" alt="right icon" />}
        />
      </div>
      <div className="mx-auto w-[600px]">
        <div className="text-lg w-full h-[200px] overflow-hidden">
          <div className="mb-4">
            <span className="text-4xl font-semibold">{currentItem?.name}</span>
            <span className="text-2xl ml-5">{currentItem?.age} tuổi</span>
          </div>
          <ul className="text-lg">
            <li>
              <Image preview={false} src="/home-icon.png" alt="home icon" />
              <span className="ml-2">Sống ở: {currentItem?.address}</span>
            </li>
            <li>
              <Image preview={false} src="/location.png" alt="Location" />
              <span className="ml-2">Quê quán: {currentItem?.hometown}</span>
            </li>
            <li className="ml-6">Sở thích: {currentItem?.hobbies}</li>
            <li className="ml-6">Ngành học: {currentItem?.major}</li>
          </ul>
        </div>
        {!props.hideControlBar && (
          <div className="flex justify-around items-center mt-2">
            <Tooltip title="Bỏ qua">
              <NextImage
                onClick={nextItem}
                className="cursor-pointer"
                width={62}
                height={62}
                src="/close.png"
                alt="close icon"
              />
            </Tooltip>
            <Tooltip title="Thêm crush">
              <NextImage
                className="cursor-pointer"
                width={62}
                height={62}
                src="/tym.png"
                alt="tym icon"
                onClick={() => handleRequestRelationship(true, currentItem.id)}
              />
            </Tooltip>
            <Tooltip title="Thêm bạn bè">
              <NextImage
                className="cursor-pointer"
                width={62}
                height={62}
                src="/add.png"
                alt="add icon"
                onClick={() => handleRequestRelationship(false, currentItem.id)}
              />
            </Tooltip>
          </div>
        )}
      </div>
    </Card>
  );
};

export default CardItem;
