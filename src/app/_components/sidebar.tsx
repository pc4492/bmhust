'use client';

import React from 'react';
import { Layout } from 'antd';
import Link from 'next/link';
import Image from 'next/image';
import SidebarItem from './sidebar-item';
import useRoute from '@/src/hooks/useRoute';
import { LogoutOutlined } from '@ant-design/icons';
import { signOut } from 'next-auth/react';


const { Content } = Layout;

export const Sidebar = ({ children }: { children: React.ReactNode }) => {
  const routes = useRoute();

  return (
    <Layout className="h-screen" hasSider>
      <div id="side-bar" className="group border border-l-black bg-white overflow-hidden w-[76px] hover:w-[220px] duration-500 transition-all">
        <Link
          href={'/'}
          className="group flex items-center gap-x-3 rounded-md p-3 text-sm leading-6 hover:text-black hover:bg-gray-100"
        >
          <Image src="/logo.svg" height={50} width={50} alt="logo" />
          <span className="group-hover:block hidden text-primary text-lg font-bold whitespace-nowrap">Bà mai HUST</span>
        </Link>

        {routes.map((route, index) => (
          <SidebarItem key={index} {...route} />
        ))}
        <SidebarItem
          label="Đăng xuất"
          icon={LogoutOutlined}
          href="#"
          onClick={() => signOut()}
        />
      </div>
      <Content className="bg-white">{children}</Content>
    </Layout>
  );
};
