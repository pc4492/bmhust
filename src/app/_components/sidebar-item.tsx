
import Link from 'next/link';
import React from 'react';
import cn from 'classnames';

type SidebarItemProps = {
  label: string;
  icon?: any;
  href: string;
  onClick?: () => void;
  active?: boolean;
}

const SidebarItem: React.FC<SidebarItemProps> = ({
  label,
  icon: Icon,
  href,
  onClick,
  active,
}) => {
  const handleClick = () => {
    if (onClick) return onClick();
  };

  return (
    <Link
    onClick={handleClick}
    href={href}
    className={cn(
      'group flex items-center gap-x-3 rounded-md p-3 text-sm leading-6 hover:text-black hover:bg-gray-100',
      { 'text-primary': active }
    )}
  >
    <Icon className={cn('h-6 w-6 text-xl shrink-0 hover:text-slate-300 ml-3',  active ? '!text-primary': '!text-slate-800')} />
    <span className={cn('side-bar-item group-hover:block hidden hover:text-slate-300 whitespace-nowrap', active ? '!text-primary font-bold': '!text-slate-800')}>{label}</span>
  </Link>
  );
};

export default SidebarItem;
