import { pusherServer } from '@/lib/pusher';
import getCurrentUser from '@/src/actions/getCurrentUser';
import { NextResponse } from 'next/server';

export async function GET(req: Request, res: Response) {
  const currentUser = await getCurrentUser();
  const { searchParams } = new URL(req.url);
  const receiverId = searchParams.get('receiverId');
  const roomName = searchParams.get('roomName');

  if (!receiverId || !roomName) {
    return new NextResponse('Bad request', { status: 400 });
  }
  if (!currentUser?.id) {
    return new NextResponse('Unauthorized', { status: 401 });
  }

  try {
    await pusherServer.trigger(roomName, 'videocall:camera:open_request', {
      senderId: currentUser.id,
    });
    return new NextResponse(null, { status: 200 });
  } catch (err) {
    console.error(err);
    return new NextResponse('Internal server error', { status: 500 });
  }
}
