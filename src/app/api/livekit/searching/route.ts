import prisma from '@/lib/prisma';
import { getRoomName } from '@/src/utils/videocall';
import { NextRequest, NextResponse } from 'next/server';

const apiKey = process.env.LIVEKIT_API_KEY;
const apiSecret = process.env.LIVEKIT_API_SECRET;
const wsUrl = process.env.NEXT_PUBLIC_LIVEKIT_URL;

/**
 * Save info waiting person to db -> waiting for call
 * @param req
 * @param res
 * @returns
 */
export async function POST(req: NextRequest) {
  try {
    if (req.method !== 'POST') {
      return NextResponse.json(
        { error: 'Method not allowed' },
        { status: 405 }
      );
    }
    const body = await req.json();
    const { userEmail } = body;

    if (!userEmail) {
      return NextResponse.json(
        { error: 'Missing "userEmail" parameter in body' },
        { status: 400 }
      );
    }

    const currentUser = await prisma.user.findUnique({
      where: { email: userEmail },
    });

    if (!currentUser) {
      return NextResponse.json(
        { error: 'You do not have permission' },
        { status: 403 }
      );
    }

    // If already exists
    const currentUserCall = await prisma.lookingForCalls.findFirst({
      where: { callerId: currentUser.id },
      orderBy: {
        startTime: 'desc',
      },
    });
    if (currentUserCall) {
      return NextResponse.json(
        { newLookingForCall: currentUserCall },
        { status: 201 }
      );
    }

    const newLookingForCall = await prisma.lookingForCalls.create({
      data: {
        callerId: currentUser.id,
        status: 0,
      },
    });

    return NextResponse.json(
      { newLookingForCall: newLookingForCall },
      { status: 201 }
    );
  } catch (error) {
    console.log('[LOOKING_FOR_CALL_POST error]', error);
    return NextResponse.json({ error: 'Internal Error' }, { status: 500 });
  }
}

/**
 * Get roomName for call after matched for call -> stop waiting
 * @param req
 * @returns
 */
export async function GET(req: NextRequest) {
  try {
    const userEmail = req.nextUrl.searchParams.get('userEmail');

    if (!userEmail) {
      return NextResponse.json(
        { error: 'Missing "userEmail" query parameter' },
        { status: 400 }
      );
    }

    const currentUser = await prisma.user.findUnique({
      where: { email: String(userEmail) },
    });

    if (!currentUser) {
      return NextResponse.json(
        { error: 'You do not have permission' },
        { status: 403 }
      );
    }

    const latestCallOfCurrentUser = await prisma.lookingForCalls.findFirst({
      where: {
        callerId: currentUser.id,
      },
      orderBy: {
        startTime: 'desc',
      },
    });

    if (!latestCallOfCurrentUser) {
      return NextResponse.json({ error: 'Call not found' }, { status: 400 });
    }

    // ADD LOGIC FIND MATCHED USER

    // simple logic: match with first waiting user, calls not from current user
    const latestCallNotFromCurrentUser = await prisma.lookingForCalls.findFirst(
      {
        where: {
          callerId: {
            not: {
              equals: String(currentUser.id),
            },
          },
          OR: [
            // calls in WAITING state
            {
              status: 0,
              matchedCallId: null,
            },
            // calls in SUCCESS state, matched with latest call of current user
            {
              status: 1,
              matchedCallId: latestCallOfCurrentUser.id,
            },
          ],
        },
        orderBy: {
          startTime: 'desc',
        },
      }
    );

    if (!latestCallNotFromCurrentUser) {
      // No matched user
      return new Response(null, { status: 204 });
    }

    // Update status and matchedCallId for 2 User
    if (
      latestCallNotFromCurrentUser.status === 0 &&
      latestCallOfCurrentUser.status === 0
    ) {
      await prisma.$transaction([
        prisma.lookingForCalls.update({
          where: { id: latestCallOfCurrentUser.id },
          data: {
            status: 1,
            matchedCallId: latestCallNotFromCurrentUser.id,
          },
        }),
        prisma.lookingForCalls.update({
          where: { id: latestCallNotFromCurrentUser.id },
          data: {
            status: 1,
            matchedCallId: latestCallOfCurrentUser.id,
          },
        }),
      ]);
    }

    // Each time update 2 User -> no have case 1 user status 0 and 1 user status 1
    // Just have: 2 user status 0, or 2 User status 1
    // if (latestCallNotFromCurrentUser.status === 0 || latestCallOfCurrentUser.status === 0) {
    //   return NextResponse.json({ error: 'Could have error' }, { status: 400 });
    // }

    const roomName = getRoomName(
      latestCallOfCurrentUser.id.toString(),
      latestCallNotFromCurrentUser.id.toString(),
    );

    // console.log(roomName);
    return NextResponse.json({
      roomName,
      matchedUserId: latestCallNotFromCurrentUser.callerId
    }, { status: 200 });
  } catch (error) {
    console.log('[MESSAGES_POST]', error);
    return NextResponse.json({ message: 'Internal Error' }, { status: 500 });
  }
}

/**
 * Cancel searching
 * @param req
 * @returns
 */
export async function DELETE(req: NextRequest) {
  try {
    const userEmail = req.nextUrl.searchParams.get('userEmail');

    /**
     * Check params
     */
    if (!userEmail) {
      return NextResponse.json(
        { error: 'Missing "username" query parameter' },
        { status: 400 }
      );
    }
    if (!apiKey || !apiSecret || !wsUrl) {
      return NextResponse.json(
        { error: 'Server misconfigured' },
        { status: 500 }
      );
    }

    // const roomService = new RoomServiceClient(wsUrl, apiKey, apiSecret);

    const currentUser = await prisma.user.findUnique({
      where: { email: userEmail },
    });
    if (!currentUser) {
      return NextResponse.json(
        { error: 'User not found' },
        { status: 400 }
      );
    }

    const call = await prisma.lookingForCalls.findFirst({
      where: {
        callerId: currentUser.id,
        status: 0,
      },
    });
    if (!call) {
      return NextResponse.json({ error: 'Call not found' }, { status: 404 });
    }

    await prisma.lookingForCalls.delete({
      where: {
        id: call.id,
      },
    });

    return NextResponse.json({ message: 'Success' }, { status: 200 });

  } catch (error) {
    console.log('[MESSAGES_POST]', error);
    return NextResponse.json({ message: 'Internal Error' }, { status: 500 });
  }
}
