import prisma from '@/lib/prisma';
import { AccessToken, RoomServiceClient, Room } from 'livekit-server-sdk';
import { NextRequest, NextResponse } from 'next/server';

const apiKey = process.env.LIVEKIT_API_KEY;
const apiSecret = process.env.LIVEKIT_API_SECRET;
const wsUrl = process.env.NEXT_PUBLIC_LIVEKIT_URL;

/**
 * Retrieve token for user to join room
 * @param req
 * @returns
 */
export async function GET(req: NextRequest) {
  try {
    const room = req.nextUrl.searchParams.get('room');
    const userEmail = req.nextUrl.searchParams.get('userEmail');
    if (!room) {
      return NextResponse.json(
        { error: 'Missing "room" query parameter' },
        { status: 400 }
      );
    } else if (!userEmail) {
      return NextResponse.json(
        { error: 'Missing "username" query parameter' },
        { status: 400 }
      );
    }

    if (!apiKey || !apiSecret || !wsUrl) {
      return NextResponse.json(
        { error: 'Server misconfigured' },
        { status: 500 }
      );
    }

    const roomService = new RoomServiceClient(wsUrl, apiKey, apiSecret);

    await roomService.createRoom({
      name: room,
      emptyTimeout: 3 * 60, // 3 minutes
      maxParticipants: 2,
    });

    const at = new AccessToken(apiKey, apiSecret, {
      identity: userEmail,
      ttl: '30m',
    });

    at.addGrant({ room, roomJoin: true, canPublish: true, canSubscribe: true });

    return NextResponse.json({ token: at.toJwt() });
  } catch (error) {
    console.log('[LIVEKIT_GET_TOKEN]', error);
    return NextResponse.json({ message: 'Internal Error' }, { status: 500 });
  }
}

/**
 * User leave room => delete room
 * @param req
 * @returns
 */
export async function DELETE(req: NextRequest) {
  try {
    const room = req.nextUrl.searchParams.get('room');
    const userEmail = req.nextUrl.searchParams.get('userEmail');
    const time = Number(req.nextUrl.searchParams.get('time'));  // Calling time left (seconds)

    if (!room) {
      return NextResponse.json(
        { error: 'Missing "room" query parameter' },
        { status: 400 }
      );
    } else if (!userEmail) {
      return NextResponse.json(
        { error: 'Missing "username" query parameter' },
        { status: 400 }
      );
    } else if (!time || Number.isNaN(time) || time < 0 || !Number.isInteger(time)) {
      return NextResponse.json(
        { error: 'Missing "time" query parameter' },
        { status: 400 }
      );
    }

    if (!apiKey || !apiSecret || !wsUrl) {
      return NextResponse.json(
        { error: 'Server misconfigured' },
        { status: 500 }
      );
    }

    const roomService = new RoomServiceClient(wsUrl, apiKey, apiSecret);
    const listParticipants = await roomService.listParticipants(room);
    roomService.deleteRoom(room);

    const currentUser = await prisma.user.findUnique({
      where: { email: userEmail },
    });
    if (!currentUser) {
      return NextResponse.json(
        { error: 'May be have error 1' },
        { status: 400 }
      );
    }

    const latestCallOfCurrentUser = await prisma.lookingForCalls.findFirst({
      where: { callerId: currentUser.id },
      orderBy: { startTime: 'desc' },
    });
    if (!latestCallOfCurrentUser) {
      // Both of lookingForCall histories are deleted
      return new NextResponse(null, { status: 200 });
    }

    /**
     * Remove the entries corresponding to userEmail from db
     */
    await prisma.lookingForCalls.delete({
      where: {
        id: latestCallOfCurrentUser.id,
      },
    });
    if (latestCallOfCurrentUser.matchedCallId) {
      await prisma.lookingForCalls.delete({
        where: { id: latestCallOfCurrentUser.matchedCallId },
      });
    }

    /**
     * Create videoCallLog entry
     */
    const otherUser = listParticipants.find(
      (item) => item.identity !== currentUser.name
    );
    if (!otherUser) {
      return NextResponse.json({ error: 'End call' }, { status: 200 });
    }

    const otherUserInfo = await prisma.user.findUnique({
      where: { email: otherUser.identity },
    });
    if (!otherUserInfo) {
      return NextResponse.json(
        { error: 'May be have error 2' },
        { status: 400 }
      );
    }
    const videoCallLog = await prisma.videoCallLog.create({
      data: {
        turnOffUser: currentUser.id,
        matchedUser: String(otherUserInfo?.id),
        leftTime: time,
      },
    });

    return NextResponse.json({ videoCallLog });
  } catch (error) {
    console.log('[LIVEKIT_DELETE]', error);
    return NextResponse.json({ message: 'Internal Error' }, { status: 500 });
  }
}
