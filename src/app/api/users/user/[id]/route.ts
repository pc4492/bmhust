import prisma from '@/lib/prisma';
import { NextResponse } from 'next/server';

/**
 *
 * @param req id
 * @returns User
 */
type Params = {
  id: string;
};
export async function GET(request: Request, { params }: { params: Params }) {
  const {id} = params;

  const user = await prisma.user.findUnique({
    where: {
      id: id as string,
    },
    include: {
      images: { select: { id: true, url: true } },
      hobbies: true,
    },
  });

  return NextResponse.json(user);
}
