import prisma from '@/lib/prisma';
import getCurrentUser from '@/src/actions/getCurrentUser';
import { User, getServerSession } from 'next-auth';
import { NextResponse } from 'next/server';

/**
 * search and filter user info
 * @param user email
 */
export async function GET(request: Request, response: Response) {
  const currentUser = await getCurrentUser();
  // if (!currentUser) {
  //   return new NextResponse('Forbidden', { status: 403 });
  // }

  const { searchParams } = new URL(request.url);
  const gender = searchParams.get('gender');
  const hometown = searchParams.get('hometown');
  const major = searchParams.get('major');
  const hobbies = searchParams.get('hobbies');
  const semester = searchParams.get('semester');

  try {
    const partners = await prisma.user.findMany({
      where: {
        id: {
          not: currentUser?.id, // Not contain current user
        },
        gender: gender
          ? {
              in: gender.split(','),
            }
          : undefined,
        hometown: hometown
          ? {
              in: hometown.split(','),
            }
          : undefined,
        major: major
          ? {
              in: major.split(','),
            }
          : undefined,
        hobbies: hobbies
          ? {
              some: {
                id: {
                  in: hobbies.split(',').map((item: string) => +item),
                },
              },
            }
          : undefined,
        grade: semester ? +semester : undefined,
      },
      include: {
        images: { select: { id: true, url: true } },
        hobbies: true,
      },
      orderBy: {
        email: 'asc',
      },
    });
    return NextResponse.json(partners);
  } catch (err) {
    console.error(err);
    return new NextResponse('Internal server error', { status: 500 });
  }
}
