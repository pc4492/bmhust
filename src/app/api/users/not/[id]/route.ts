import prisma from '@/lib/prisma';
import { NextResponse } from 'next/server';

type Params = {
  id: string;
};

export async function GET(req: Request, { params } : { params: Params }) {
  const {id} = params;
  
  const user = await prisma.user.findMany({
    where: {
      id: {not: id as string},
    },
  });
  
  return NextResponse.json(user);
}
