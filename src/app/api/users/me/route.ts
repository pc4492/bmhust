import prisma from '@/lib/prisma';
import { User, getServerSession } from 'next-auth';
import { NextResponse } from 'next/server';
import getCurrentUser from '@/src/actions/getCurrentUser';

/**
 * get current user information
 * @param user email
 * @returns
 */
export async function GET() {
  const user = await getCurrentUser();
  if (!user || !user.email) {
    return NextResponse.json(
      { error: 'The request has not been applied because it lacks valid authentication credentials' },
      { status: 401 }
    );
  }

  const userInDb = await prisma.user.findUnique({
    where: { email: user.email},
  });
  if (!userInDb) {
    return NextResponse.json(
      { error: 'The request has not been applied because it lacks valid authentication credentials' },
      { status: 401 }
    );
  }
  return NextResponse.json(userInDb);
}
