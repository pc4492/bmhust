import prisma from '@/lib/prisma';
import { NextResponse } from 'next/server';

/**
 * Get all hobbies
 * 
 */
export async function GET(request: Request, response: Response) {
  try {
    const hobbies = await prisma.hobbies.findMany();
    return NextResponse.json(hobbies);
  } catch (err) {
    console.error(err);
    return new NextResponse('Internal server error', { status: 500 });
  }
};
