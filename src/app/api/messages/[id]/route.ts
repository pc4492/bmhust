import prisma from '@/lib/prisma';
import { NextResponse } from 'next/server';

/**
 * 
 * @param req id
 */
type Params = {
  id: string;
};
export async function GET(request: Request, { params }: { params: Params }) {
  const {id} = params;
  const _id = parseInt(id as string);
    
  if (Number.isNaN(_id)) {
    return new NextResponse('Invalid ID', { status: 400 });
  }

  const user = await prisma.messages.findUnique({
    where: {
      id: _id,
    },
    include: {
      sender: true,
      receiver: true
    }
  });

  return NextResponse.json(user);
}
