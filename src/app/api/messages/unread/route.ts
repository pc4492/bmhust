import getCurrentUser from '@/src/actions/getCurrentUser';
import { NextResponse } from 'next/server';
import prisma from '@/lib/prisma';

/**
 * Get number of unread messages
 * @param req
 * @param res
 * @returns
 */
export async function GET(req: Request, res: Response) {
  try {
    const currentUser = await getCurrentUser();
    if (!currentUser) {
      return new NextResponse('Unauthorized', { status: 401 });
    }

    const nUnread = await prisma.messages.count({
      where: {
        OR: [{ senderId: currentUser.id }, { receiverId: currentUser.id }],
        NOT: {
          seen: {
            has: currentUser.id,
          },
        },
      },
    });

    return NextResponse.json(nUnread);
  } catch (err) {
    console.error(err);
    return new NextResponse('Internal server error', { status: 500 });
  }
}
