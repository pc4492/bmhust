import getCurrentUser from '@/src/actions/getCurrentUser';
import { NextResponse } from 'next/server';
import prisma from '@/lib/prisma';
import { pusherServer } from '@/lib/pusher';
import { getConversationName } from '@/src/utils/chat';

type Params = {
  partnerId: string;
};

/**
 * Get last message
 * @param req 
 * @param param1 
 * @returns 
 */
export async function GET(req: Request, { params } : { params: Params }) {
  try {
    const {partnerId} = params;
    const currentUser = await getCurrentUser();
    if (!currentUser) {
      return new NextResponse('Unauthorized', { status: 401 });
    }
    const lastMessage = await prisma.messages.findFirst({
      where: {
        OR:[
          {
            senderId: currentUser.id,
            receiverId: partnerId as string,
          },{
            senderId: partnerId as string,
            receiverId: currentUser.id,
          }
        ],
      },
      orderBy:{
        createdAt:'desc'
      },
    });
    return NextResponse.json(lastMessage);
  } catch(err) {
    console.error(err);
    return new NextResponse('Internal server error', { status: 500 });
  }
}

/**
 * Update seen status of lastest message
 * @param req
 */
export async function PUT(req: Request, { params } : { params: Params }) {
  try {
    const {partnerId} = params;
    const currentUser = await getCurrentUser();
    if (!currentUser) {
      return new NextResponse('Unauthorized', { status: 401 });
    }
    const lastMessage = await prisma.messages.findFirst({
      where: {
        OR:[
          {
            senderId: currentUser.id,
            receiverId: partnerId as string,
          },{
            senderId: partnerId as string,
            receiverId: currentUser.id,
          }
        ],
      },
      orderBy:{
        createdAt:'desc'
      },
    });
    if (!lastMessage) {
      return new NextResponse(null, { status: 204 });
    }

    const updatedMessage = await prisma.messages.update({
      where: {
        id: lastMessage.id
      },
      data: {
        seen: lastMessage.seen.includes(currentUser.id) ? lastMessage.seen : [...lastMessage.seen, currentUser.id],
      },
    });

    const conversation = getConversationName(currentUser.id, partnerId);
    pusherServer.trigger(conversation, 'messages:seen', updatedMessage);

    return new NextResponse(null, { status: 200 });
  } catch(err) {
    console.error(err);
    return new NextResponse('Internal server error', { status: 500 });
  }
}
