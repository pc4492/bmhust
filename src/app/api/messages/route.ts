import prisma from '@/lib/prisma';
import { pusherServer } from '@/lib/pusher';
import getCurrentUser from '@/src/actions/getCurrentUser';
import { getConversationName } from '@/src/utils/chat';
import { NextResponse } from 'next/server';

/**
 * Find messages
 * @param req 
 *  query: {
 *   senderId: number,
 *   receiverId: number,
 *  }
 *  
 * @returns new message
 */
export async function GET(request: Request, response: Response) {
  const {searchParams} = new URL(request.url);
  const senderId = searchParams.get('senderId');
  const receiverId = searchParams.get('receiverId');

  try {
    const messages = await prisma.messages.findMany({
      where: {
        OR:[
          {
            senderId: senderId as string,
            receiverId: receiverId as string,

          },{
            senderId: receiverId as string,
            receiverId: senderId as string,
          }
        ],
      },
      include: {
        sender: true,
        receiver: true
      },
      orderBy:{
        createdAt:'asc'
      }
    });

    return NextResponse.json(messages);
  } catch(err) {
    console.error(err);
    return new NextResponse('Internal server error', { status: 500 });
  }
}

/**
 * Create messages
 * @param req 
 *  body: {
 *   senderId: number,
 *   receiverId: number,
 *   content: number
 *  }
 * @returns new message
 */
export async function POST(request: Request) {
  try {
    const currentUser = await getCurrentUser();
    const body = await request.json();
    const { content, receiverId } = body;
  
    if (!(currentUser?.id)) {
      return new NextResponse('Unauthorized', { status: 401 });
    }
  
    const newMessage = await prisma.messages.create({
      data: {
        senderId: currentUser.id,
        receiverId,
        content,
        seen: [currentUser.id],
      },
      include: {
        sender: true,
        receiver: true
      }
    });
  
    console.log('[MESSAGES_POST]', newMessage);
    const conversation = getConversationName(body.senderId, body.receiverId);
    await pusherServer.trigger(conversation, 'messages:new', newMessage);
    
    return NextResponse.json(newMessage);
  } catch(err) {
    console.log('[MESSAGES_POST]', err);
    return new NextResponse('Internal Error', { status: 500 });
  }
} 
