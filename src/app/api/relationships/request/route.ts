import prisma from '@/lib/prisma';
import { NextRequest, NextResponse } from 'next/server';
import { User, getServerSession } from 'next-auth';
import getCurrentUser from '@/src/actions/getCurrentUser';

export async function POST(req: NextRequest) {
  try {
    if (req.method !== 'POST') {
      return NextResponse.json(
        { error: 'Method not allowed' },
        { status: 405 }
      );
    }

    const currentUser = await getCurrentUser();
    const body = await req.json();
    const { receiverId, isCrush } = body;

    if (!receiverId || isCrush === null) {
      return NextResponse.json(
        { error: 'Missing parameter in body' },
        { status: 400 }
      );
    }
    if (!currentUser || !currentUser?.id) {
      return new NextResponse('Unauthorized', { status: 401 });
    }

    // check exists relationship
    const relationship = await prisma.relationship.findFirst({
      where: {
        OR: [
          {
            requesterId: currentUser.id,
            receiverId,
          },
          {
            requesterId: receiverId,
            receiverId: currentUser.id,
          }
        ]
      }
    });

    if (relationship) {
      return NextResponse.json(
        { error: 'Already have a relationship' },
        { status: 409 }
      );
    }


    const newRelationShip = await prisma.relationship.create({
      data: {
        receiverId,
        requesterId: currentUser.id,
        isCrush,
        status: 0, // PENDING
        time: new Date(),
      },
    });

    return NextResponse.json({ newRelationShip }, { status: 201 });
  } catch (error) {
    console.log('[RELATIONSHIP REQUEST POST error]', error);
    return NextResponse.json({ error: 'Internal Error' }, { status: 500 });
  }
}

/**
 *
 * @returns GET request relationship (status = PENDING) of current user
 */
export async function GET(req: NextRequest) {
  try {
    const user = await getCurrentUser();
    if (!user || !user.email) {
      return NextResponse.json(
        {
          error:
            'The request has not been applied because it lacks valid authentication credentials',
        },
        { status: 401 }
      );
    }

    const userInDb = await prisma.user.findUnique({
      where: {
        email: user.email,
      },
      include: {
        receivedRelationships: {
          include: {
            requester: {
              include: {
                images: true,
              },
            },
          },
        },
      },
    });
    if (!userInDb) {
      return NextResponse.json(
        {
          error:
            'The request has not been applied because it lacks valid authentication credentials',
        },
        { status: 401 }
      );
    }

    const pendingRelationships = userInDb.receivedRelationships?.filter(item => item.status === 0);

    return NextResponse.json(pendingRelationships, { status: 200 });
  } catch (error) {
    console.log('[RELATIONSHIP REQUEST GET error]', error);
    return NextResponse.json({ error: 'Internal Error' }, { status: 500 });
  }
}
