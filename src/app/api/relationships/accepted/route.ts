import prisma from '@/lib/prisma';
import getCurrentUser from '@/src/actions/getCurrentUser';
import { User } from '@prisma/client';
import { NextResponse } from 'next/server';

/**
 * search and filter user info
 * @param user email
 */
export async function GET(request: Request, response: Response) {
  try {
    const currentUser = await getCurrentUser();

    const { searchParams } = new URL(request.url);
    const isCrush = searchParams.get('isCrush') === 'true';

    if (!currentUser?.id) {
      return new NextResponse('Unauthorized', { status: 401 });
    }

    const relationships = await prisma.relationship.findMany({
      where: {
        OR: [
          {
            receiverId: currentUser.id,
            status: 1,
          },
          {
            requesterId: currentUser.id,
            status: 1,
          },
        ],
      },
      include: {
        receiver: {
          include: { images: true },
        },
        requester: {
          include: { images: true },
        },
      },
    });

    let partners: User[] = [];

    relationships.map((item) => {
      if (item.isCrush === isCrush) {
        item.requesterId !== currentUser.id
          ? partners.push(item.requester) // if user is receiver
          : partners.push(item.receiver); // if user is requester
      }
    });

    return NextResponse.json(partners);
  } catch (err) {
    console.error(err);
    return new NextResponse('Internal server error', { status: 500 });
  }
}
