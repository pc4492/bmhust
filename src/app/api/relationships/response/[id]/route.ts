import prisma from '@/lib/prisma';
import { NextRequest, NextResponse } from 'next/server';

type Params = {
  id: number;
};

/**
 * Update status relationship 0 -> 1: Accept
 * @returns updated relationship
 */
export async function PATCH(request: Request, { params }: { params: Params }) {
  try {
    const { id } = params;
    const _id = Number(id);

    if (Number.isNaN(_id)) {
      return new NextResponse('Invalid ID', { status: 400 });
    }

    const relationship = await prisma.relationship.update({
      where: {
        id: _id,
      },
      data: {
        status: 1,
      },
    });

    return NextResponse.json(relationship, { status: 200 });
  } catch (error) {
    console.error(error);
    return NextResponse.json({ message: 'Internal Error' }, { status: 500 });
  }
}

/**
 * User leave room => delete room
 * @param req
 * @returns
 */
export async function DELETE(req: NextRequest, { params }: { params: Params }) {
  try {
    const { id } = params;
    const _id = Number(id);

    if (Number.isNaN(_id)) {
      return new NextResponse('Invalid ID', { status: 400 });
    }

    const deleted = await prisma.relationship.delete({
      where: {
        id: _id,
      },
    });

    return NextResponse.json(deleted, { status: 200 });
  } catch (error) {
    console.log('[LIVEKIT_DELETE]', error);
    return NextResponse.json({ message: 'Internal Error' }, { status: 500 });
  }
}
