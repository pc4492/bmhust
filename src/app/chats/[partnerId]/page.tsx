'use client';

import { PhoneFilled, VideoCameraFilled } from '@ant-design/icons';
import { User } from '@prisma/client';
import { Avatar, Badge } from 'antd';
import { useSession } from 'next-auth/react';
import { useEffect, useState } from 'react';
import ChatBox, { ExtendedMessage } from '../_components/chat-box';
import { Partner } from '../../page';

const getMesssages = async (senderId: string, receiverId: string) => {
  return fetch(`/api/messages?senderId=${senderId}&receiverId=${receiverId}`).then((res) => res.json());
};

export default function ChatPartner({ params }: { params: { partnerId: string } }) {
  // const session = await getServerSession();
  // const session = useSession();
  const [messages, setMessages] = useState<ExtendedMessage[]>();
  const [partner, setPartner] = useState<Partner>();
  const [isLoading, setIsLoading] = useState(true);
  const [error, setError] = useState<any>();
  const [currentUserId, setCurrentUserId] = useState<string>('');

  useEffect(() => {
    (async () => {
      // if (session.status !== 'authenticated') return;
      try {
        setIsLoading(true);
        const currentUserId_t = await fetch(`/api/users/me`).then((res) => res.json());
        setCurrentUserId(currentUserId_t.id);
        const m = await getMesssages(currentUserId_t.id, params.partnerId);
        const p = await fetch(`/api/users/user/${params.partnerId}`).then((res) => res.json());
        setMessages(m);
        setPartner(p);
        setError(null);
      } catch (err) {
        console.error(err);
        setError(err);
      } finally {
        setIsLoading(false);
      }
    })();
  }, [currentUserId]);

  if (isLoading || messages?.length === 0) {
    return (
      <div className="flex flex-col flex-1 items-center justify-center">
        <p className="text-2xl font-semibold">Đang tải...</p>
      </div>
    );
  }

  if (error) {
    return (
      <div className="flex flex-col flex-1 items-center justify-center">
        <p className="text-2xl font-semibold">Đã có lỗi xảy ra</p>
        <p className="text-lg font-light">Vui lòng thử lại sau</p>
      </div>
    );
  }

  return <div className="flex flex-col flex-1">
    <div className="flex flex-row items-center gap-2 border-b p-4">
      <Badge dot offset={[-5, 33]} size="default" color="green">
        <Avatar src={partner?.image || partner?.images[0]?.url} size={'large'} />
      </Badge>
      <div className="flex flex-col py-1">
        <p className="font-semibold">{partner?.name}</p>
        <p className="text-xs font-light">{'Đang hoạt động'}</p>
      </div>
      <span className="flex-1" />
      <span className="rounded-full hover:bg-primary/20 px-4 py-3 cursor-pointer">
        <PhoneFilled className="!text-primary !text-xl" />
      </span>
      <span className="rounded-full hover:bg-primary/20 px-4 py-3 cursor-pointer">
        <VideoCameraFilled className="!text-primary !text-xl" />
      </span>
    </div>
    <ChatBox messages={messages!} receiverId={params.partnerId} />
  </div>;
}
