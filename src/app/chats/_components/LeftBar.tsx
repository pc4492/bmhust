import { PlusOutlined } from '@ant-design/icons';
import { Badge } from 'antd';
import TabItem from './TabItem';
import { useEffect, useState } from 'react';

export default function LeftBar() {

  const [nUnread, setNUnread] = useState(0);
  const [loading, setLoading] = useState(true);

  useEffect(() => {
    (async () => {
      try {
        setLoading(true);
        const res = await fetch('/api/messages/unread');
        const data = await res.json();
        setNUnread(data.nUnread);
      } catch (err) {
        console.error(err);
      } finally {
        setLoading(false);
      }
    })();
  }, []);

  return (
    <div className="overflow-auto w-[416px] border">
      <div className="p-4 flex flex-row items-center">
        <Badge count={(loading && nUnread > 0) ? nUnread : undefined} className="z-2">
          <p className="text-2xl font-bold">Tin nhắn</p>
        </Badge>
        {/* <span className="bg-primary/20 shadow w-8 rounded-full cursor-pointer h-8 flex items-center justify-center">
          <PlusOutlined style={{ strokeWidth: 4 }} className="!text-primary" />
        </span> */}
      </div>
      {/* Friends list */}
      <TabItem />
    </div>
  );
}
