'use client';

import { pusherClient } from '@/lib/pusher';
import { getConversationName } from '@/src/utils/chat';
import { SendOutlined } from '@ant-design/icons';
import { Messages, User } from '@prisma/client';
import { Avatar, Input, Tooltip, Form } from 'antd';
import { useSession } from 'next-auth/react';
import { Fragment, useEffect, useRef, useState } from 'react';
import { find } from 'lodash';

export type ExtendedMessage = Messages & { sender: User; receiver: User };
type Props = {
	receiverId: string;
	messages: ExtendedMessage[];
};

export default function ChatBox(props: Props) {
	// const { data: session, status } = useSession();
	const [form] = Form.useForm();
  const messageContainerRef = useRef<HTMLDivElement>(null);

	const [messages, setMessages] = useState<typeof props.messages>(
		props.messages
	);
    const [currentUser, setCurrentUser] = useState<any>({});
  function validateInput(input: string) {
    const trimmedInput = input.trim();
    return trimmedInput !== '';
  }

    console.log(messages);
	const onFinish = (values: any) => {
    const input = values?.inputMessage || '';
    if (!validateInput(input)) {
      form.setFieldValue('inputMessage', '');
      return;
    }
    handleSendMessage(input.trim());
		form.resetFields();
	};

    useEffect(() => {
        (async () => {
            try {
                const user = await fetch(`/api/users/me`).then((res) => res.json());
                if (user) {
                    setCurrentUser(user);
                }
            } catch (err) {
                console.error(err);
            }
        })();
    }, []);
  useEffect(() => {
    // update seen status of lastest message
    fetch(`/api/messages/last/${props.receiverId}`, {method: 'PUT'});
    
    // subscribe to new message event
    const conversation = getConversationName(currentUser.id, props.receiverId);
    pusherClient.subscribe(conversation);
    messageContainerRef?.current?.scrollIntoView({ behavior: 'smooth' });

    const newMessageHandler = (message: ExtendedMessage) => {
      console.log('newMessageHandler', message);
      if(message.receiverId === currentUser.id) {
        fetch(`/api/messages/last/${message.senderId}`, {method: 'PUT'});
      }
      setMessages((current) => {
        if (find(current, { id: message.id })) {
          return current;
        }
        return [...current, message];
      });
      messageContainerRef?.current?.scrollIntoView({ behavior: 'smooth' });
    };
    pusherClient.bind('messages:new', newMessageHandler);

    return () => {
      pusherClient.unsubscribe(conversation);
      pusherClient.unbind('messages:new', newMessageHandler);
    };
  }, [currentUser, props.receiverId]);

	const handleSendMessage = (content: string) => {
    try {
      fetch('/api/messages', {
        method: 'POST',
        headers: {
          'Content-Type': 'application/json',
        },
        body: JSON.stringify({
          content: content,
          senderId: currentUser.id,
          receiverId: props.receiverId,
        }),
      }).then(async (response) => {
        const json = await response.json();
        setMessages([...messages, json]);
      });
    } catch (error) {
      console.error(error);
    }
	};

	return (
		<Fragment>
			<div ref={messageContainerRef} className="flex-1 flex overflow-auto px-3 py-4">
				<div className="h-[100%] w-full flex flex-col gap-2">
					{messages.map((message, index) => (
							<div
								style={{
									alignSelf:
										message.sender.name === currentUser.name
											? 'end'
											: 'inherit',
								}}
								className="w-fit"
								key={message.id}
							>
								{message.sender.id.toString() === currentUser.id ? (
									<div className="self-end">
										<p className="px-3 py-2 rounded bg-primary/10">
											{message.content}
										</p>
									</div>
								) : (
									<div className="flex flex-row  gap-2 items-center">
										{messages[index - 1]?.sender?.name !==
										message.sender.name ? (
											<Tooltip title={message.sender.name}>
												<Avatar src={message.sender.image} />
											</Tooltip>
										) : (
											<span className="h-[30px] w-[32px]"></span>
										)}
										<p className="px-3 py-2 rounded bg-primary/10">
											{message.content}
										</p>
									</div>
								)}
							</div>
						))}
				</div>
			</div>
			<div className="px-3 py-2">
				<Form form={form} onFinish={onFinish} >
					<Form.Item
						name="inputMessage"
						rules={[
							{
								message: 'Nhập ký tự trước khi gửi!',
								max: 5000,
							},
						]}
					>
						<Input
							size="large"
							placeholder="Tin nhắn..."
							onPressEnter={() => form.submit()}
							suffix={
								<SendOutlined
									onClick={() => form.submit()}
                  className="text-primary cursor-pointer"
								/>
							}
						/>
					</Form.Item>
				</Form>
			</div>
		</Fragment>
	);
}
