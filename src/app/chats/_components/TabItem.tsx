import { SearchOutlined } from '@ant-design/icons';
import { User, Messages } from '@prisma/client';
import { Avatar, Badge, Empty, Input, Skeleton } from 'antd';
import cn from 'classnames';
import Link from 'next/link';
import { usePathname } from 'next/navigation';
import { Fragment, useEffect, useState } from 'react';
import { Partner } from '../../page';
import { formatDistance } from 'date-fns';
import { vi as vilocale } from 'date-fns/locale/vi';
import { getConversationName } from '@/src/utils/chat';
import { pusherClient } from '@/lib/pusher';
import { ExtendedMessage } from './chat-box';
import { useSession } from 'next-auth/react';
import { Skeleton as Skeleton1 } from '@/src/components/skeleton';

import styles from './tabitem.module.css';

const tabs = [
  {
    id: 'friend',
    label: 'Bạn bè',
  },
  {
    id: 'crush',
    label: 'Crush',
  },
];

export default function TabItem() {
  const [selectedTab, setSelectedTab] = useState('friend');
  // filter input
  const [filter, setFilter] = useState('');
  // data fetching state variables
  const [partners, setPartners] = useState([]);
  const [isLoading, setIsLoading] = useState(true);
  const [error, setError] = useState<any>();
  const [currentUserId, setCurrentUserId] = useState<string>('');

  useEffect(() => {
    // reset filter
    setFilter('');
    // fetch data
    (async () => {
      try {
        setIsLoading(true);
        const ret = await fetch(
          `/api/relationships/accepted?isCrush=${selectedTab === 'crush'}`
        ).then((res) => res.json());
        setPartners(ret);
        setError(null);
      } catch (err) {
        console.error(err);
        setError(err);
      } finally {
        setIsLoading(false);
      }
    })();
  }, [selectedTab]);

  useEffect(() => {
    (async () => {
      try {
        const res = await fetch('/api/users/me');
        if (res.status === 200) {
          const user = await res.json();
          setCurrentUserId(user.id);
        } else {
          throw new Error('Cannot fetch current user');
        }
      } catch (err) {
        console.error(err);
      }
    })();
  }, []);
  return (
    <Fragment>
      <div className="mt-4 px-4">
        <Input
          size="large"
          placeholder="Tìm kiếm"
          prefix={<SearchOutlined />}
          onChange={(e) => setFilter(e.target.value)}
        />
      </div>
      <div className="mt-8">
        <div className="flex mb-8 border">
          {tabs.map((tab) => (
            <div
              key={tab.id}
              className={cn(
                'px-6 py-1 cursor-pointer w-1/2 text-center',
                tab.id === selectedTab
                  ? 'bg-white text-primary'
                  : ' bg-zinc-300'
              )}
              onClick={() => setSelectedTab(tab.id)}
            >
              {tab.label}
            </div>
          ))}
        </div>
      </div>
      {error ? (
        <div className="flex flex-col flex-1 items-center justify-center">
          <p className="text-2xl font-semibold">Đã có lỗi xảy ra</p>
          <p className="text-lg font-light">Vui lòng thử lại sau</p>
        </div>
      ) : (
        <Skeleton loading={isLoading}>
          <PartnerList currentUserId={currentUserId} partners={partners} filter={filter} />
        </Skeleton>
      )}
    </Fragment>
  );
}

function PartnerList({
                       currentUserId,
                       partners,
                       filter,
                     }: {
  currentUserId: string,
  partners: Partner[],
  filter: string,
}) {
  // effective partners
  const [ePartners, setEPartners] = useState<Partner[]>(partners);

  useEffect(() => {
    setEPartners(partners.filter((user: User) =>
      user.name.toLowerCase().includes(filter.toLowerCase())
    ));
  }, [filter, partners]);

  return (
    <div className="flex flex-col p-2 gap-2 w-full">
      {(partners.length === 0)
        ? <Empty />
        : ePartners.map((p, i) => (
          <PartnerItem key={i} currentUserId={currentUserId} partner={p} />
        ))}
    </div>
  );
}

function PartnerItem({
  currentUserId,
  partner,
}: {
  currentUserId: string,
  partner: Partner
}) {
  const pathname = usePathname();

  const [lastMessage, setLastMessage] = useState<Messages | null>(null);
  const [isLoading, setIsLoading] = useState(true);
  const [error, setError] = useState<any>();

  useEffect(() => {
    // fetch last message
    (async () => {
      try {
        setIsLoading(true);
        const res = await fetch(`/api/messages/last/${partner.id}`);
        if (res.status === 200) {
          const last = await res.json();
          setLastMessage(last);
        } else {
          throw new Error('Cannot fetch last message');
        }
        // setLastMessage(ret);
      } catch (err) {
        console.error(err);
        setError(err);
      } finally {
        setIsLoading(false);
      }
    })();

    // subscribe to new message
    const conversation = getConversationName(currentUserId, partner.id);
    pusherClient.subscribe(conversation);
    console.log('subscribed to', conversation);

    const newMessageHandler = (message: ExtendedMessage) => {
      console.log('newMessageHandler', message);
      if(getConversationName(message.senderId, message.receiverId) !== conversation) return;
      console.log('Handling new message');
      setLastMessage(message);
    };
    const seenMessageHandler = (message: Messages) => {
      console.log('seenMessageHandler', message, 'channel', conversation);
      if(getConversationName(message.senderId, message.receiverId) !== conversation) return;
      console.log('Handling seen message');
      setLastMessage(message);
    };
    pusherClient.bind('messages:new', newMessageHandler);
    pusherClient.bind('messages:seen', seenMessageHandler);

    return () => {
      console.log('about to unmount', conversation);
      pusherClient.unsubscribe(conversation);
      pusherClient.unbind('messages:new', newMessageHandler);
      pusherClient.unbind('messages:seen', seenMessageHandler);
    };
  }, [currentUserId, partner.id]);

  return (
    <Link href={`/chats/${partner.id}`}>
      <div className={cn(
        'block w-full px-3 py-2 rounded-lg hover:bg-slate-100',
        pathname?.slice('/chats/'.length) === partner.id.toString()
          ? 'bg-indigo-100'
          : 'bg-white'
      )}>
        {(isLoading || error) ? (
          <div>
            <Skeleton1 className="h-12 w-12 rounded-full" />
            <div className="space-y-2">
              <Skeleton1 className="h-4 w-[250px]" />
              <Skeleton1 className="h-4 w-[200px]" />
            </div>
          </div>
        ) : (
          <div className="w-full flex flex-row gap-3 justify-between items-center">
            <div className="flex-1 flex flex-row items-center gap-2">
              <div className="w-10 h-10">
                <Avatar
                  src={partner.image || partner.images[0]?.url}
                  size={'large'}
                />
              </div>
              <div className="flex-1 flex flex-col gap-2 text-black">
                <p className="font-semibold">{partner.name}</p>
                <p className={cn('text-sm font-light overflow-hidden', styles.messagePreview)}>
                  {lastMessage
                    ? ((lastMessage.senderId === currentUserId ? 'Bạn: ' : `${partner.name.split(' ').at(-1)}: `) + lastMessage.content)
                    : `Bắt đầu chat với ${partner.name}`}
                </p>
              </div>
            </div>
            <div className="flex flex-row items-center gap-2">
              {lastMessage?.createdAt && (
                <div className="text-xs text-gray-400 font-light w-fit">
                  {formatDistance(new Date(lastMessage.createdAt), new Date(), {
                    addSuffix: true,
                    locale: vilocale,
                  })}
                </div>
              )}
              {
                (isLoading || error || !lastMessage)
                  ? null
                  : !(lastMessage.seen.includes(currentUserId))
                    ? (<span className="block rounded-full bg-red-500 h-2 w-2 md:h-3 md:w-3" />)
                    : null
              }
            </div>
          </div>
        )}
      </div>
    </Link >
  );
}
