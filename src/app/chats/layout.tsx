'use client';

import { PropsWithChildren, useEffect, useState } from 'react';
import LeftBar from './_components/LeftBar';

export default function ChatsPage({ children }: PropsWithChildren) {
  return (
    <div className="h-screen flex flex-row ">
      <LeftBar />
      {children}
    </div>
  );
}
