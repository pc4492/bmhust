'use client';

import { useSession } from 'next-auth/react';
import { useEffect } from 'react';
import { useRouter } from 'next/navigation';
import { Empty } from 'antd';

export default function ChatDefault() {
  // const session = useSession();
  const router = useRouter();

  useEffect(() => {
    const navigate = async () => {
      // if (session.status !== 'authenticated') return;

      try {
        const partners = await fetch(
          `/api/relationships/accepted?isCrush=false`
        ).then((res) => res.json());
        if (partners.length > 0) {
          router.replace(`/chats/${partners[0].id}`);
        }
      } catch (err) {
        console.error(err);
      }
    };

    navigate();
  }, []);

  return (
    <div className='flex-1 flex overflow-auto px-3 py-4 h-full w-full justify-center items-center'>
      <Empty />
    </div>
  );
}
