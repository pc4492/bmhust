'use client';

import { App, Avatar, Button, Card, Col, Empty, Row } from 'antd';
import RightPart from './_components/RightPart';
import { useEffect, useMemo, useState } from 'react';
import cn from 'classnames';
import { Relationship, User } from '@prisma/client';

const tabs = [
  {
    id: 0,
    label: 'Muốn làm bạn bè',
  },
  {
    id: 1,
    label: 'Muốn hẹn hò',
  },
];

type RelationshipEx = Relationship & {
  requester: User & {
    images: {
      id: string;
      url: string;
    }[];
  };
};

type UserEx = User & {
  receivedRelationships: RelationshipEx[];
};

export default function FriendsPage() {
  const { notification } = App.useApp();

  const [relationships, setRelationships] = useState<RelationshipEx[]>([]);
  const [selectedTab, setSelectedTab] = useState(0);
  const [selectedUser, setSelectedUser] = useState<User>();

  useEffect(() => {
    const getRelationships = async () => {
      const response = await fetch(`/api/relationships/request`);

      if (response.status === 200) {
        const list = await response.json();

        setRelationships(list);
      }
    };
    getRelationships();
  }, []);

  // select default partner
  useEffect(() => {
    if (tabRelationships && tabRelationships?.length > 0) {
      setSelectedUser(tabRelationships[0].requester);
    }
  }, [selectedTab, relationships]);

  const tabRelationships = useMemo(() => {

    return relationships?.filter(
      (item) => Boolean(selectedTab) === item.isCrush
    );
  }, [selectedTab, relationships]);

  const handleAccept = async (relationshipId: number) => {
    const response = await fetch(`/api/relationships/response/${relationshipId}`, {
      method: 'PATCH',
    });

    if (response.status === 200) {
      notification.success({ message: 'Kết bạn thành công' });
      const newRelationships = relationships.filter(item => item.id !== relationshipId);
      setRelationships(newRelationships);
    } else {
      notification.error({ message: 'Đã xảy ra lỗi' });
    }
  };

  const handleDeny = async (relationshipId: number) => {
    const response = await fetch(`/api/relationships/response/${relationshipId}`, {
      method: 'DELETE',
    });

    if (response.status === 200) {
      notification.success({ message: 'Từ chối thành  công!' });
      const newRelationships = relationships.filter(item => item.id !== relationshipId);
      setRelationships(newRelationships);
    } else {
      notification.error({ message: 'Đã xảy ra lỗi' });
    }
  };

  if (!relationships) {
    return <Card>
      <Empty />
    </Card>;
  }

  return (
    <Row className='h-screen'>
      <Col xl={6} lg={24} className='border-r-[2px]'>
        <div>
          <h3 className="text-xl font-semibold pt-8 pl-4 pb-10 ">
            Lời mời kết bạn {relationships.length || 0}
          </h3>
          <div className="flex mb-8 border">
            {tabs.map((tab) => (
              <div
                key={tab.id}
                className={cn(
                  'px-6 py-1 cursor-pointer w-1/2 text-center',
                  tab.id === selectedTab
                    ? 'bg-white text-primary'
                    : ' bg-zinc-300'
                )}
                onClick={() => setSelectedTab(tab.id)}
              >
                {tab.label}
              </div>
            ))}
          </div>
          {/* Body */}
          {tabRelationships &&
            tabRelationships.map(({ requester, id }) => (
              <div
                key={requester.id}
                className={cn(
                  'flex gap-4 items-center my-2 mx-4 p-2 rounded-lg',
                  selectedUser?.id === requester.id
                    ? 'bg-indigo-100'
                    : 'bg-white'
                )}
                onClick={() => setSelectedUser(requester)}
              >
                <div>
                  <Avatar src={requester.image || requester.images[0].url} size={50} />
                </div>
                <div className="w-full space-y-1">
                  <p>{requester.name}</p>
                  <div className="flex justify-between flex-wrap gap-2">
                    <Button
                      type="primary"
                      className="!rounded-2xl !px-6"
                      onClick={() => handleAccept(id)}
                    >
                      Xác nhận
                    </Button>

                    <Button
                      type="primary"
                      className="!rounded-2xl !px-8 !bg-slate-300"
                      onClick={() => handleDeny(id)}
                    >
                      <p className="text-black font-bold">Xóa</p>
                    </Button>
                  </div>
                </div>
              </div>
            ))}
        </div>
      </Col>
      <Col xl={18} lg={24}>
        <RightPart userId={selectedUser?.id || ''} />
      </Col>
    </Row>
  );
}
