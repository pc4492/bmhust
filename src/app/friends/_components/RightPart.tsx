'use client';

import { useEffect, useState } from 'react';
import CardItem from '../../_components/card-item';
import { Partner } from '../../page';
import { Card, Empty, Skeleton } from 'antd';

type Prop = {
  userId: string;
};

export default function RightPart({ userId }: Prop) {
  const [partner, setPartner] = useState<Partner>();
  const [isLoading, setIsLoading] = useState(true);
  const [error, setError] = useState<any>();

  useEffect(() => {
    const getPartner = async () => {
      setIsLoading(true);
      try {
        const response = await fetch(
          `/api/users/user/${userId}`
        );
        if (response.status === 200) {
          const partner = await response.json();
          setPartner(partner);
        }
        setError(null);
      } catch (error) {
        console.error(error);
        setError(error);
      }
      setIsLoading(false);
    };

    getPartner();
  }, [userId]);

  if (error) {
    return (
      <div className="flex flex-col flex-1 items-center justify-center">
        <p className="text-2xl font-semibold">Đã có lỗi xảy ra</p>
        <p className="text-lg font-light">Vui lòng thử lại sau</p>
      </div>
    );
  }

  return (
    <Skeleton loading={isLoading} className="mt-20 px-8">
      <div>
        {!partner ? (
          <Card>
            <Empty className="mt-24" />
          </Card>
        ) : (
          <CardItem
            items={[partner].map((partner: Partner) => ({
              id: partner.id,
              address: partner.address!,
              hometown: partner.hometown!,
              major: partner.major!,
              name: partner.name,
              images: partner.images,
              hobbies: partner.hobbies.map((hobby) => hobby.name).join(', '),
              age:
                partner.grade !== null
                  ? new Date().getFullYear() - (1955 - 18 + partner.grade)
                  : 'Chưa có thông tin',
            }))}
            hideControlBar={true}
          />
        )}
      </div>
    </Skeleton>
  );
}
