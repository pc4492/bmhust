import StyledComponentsRegistry from '@/lib/AntdRegistry';
import { App as AntApp } from 'antd';
import type { Metadata } from 'next';
import { Inter } from 'next/font/google';
import NextAuthProvider from './_components/provider';
import { Sidebar } from './_components/sidebar';
import './globals.css';

const inter = Inter({ subsets: ['latin'] });

export const metadata: Metadata = {
  title: 'Ba mai HUST',
  description: 'ITSS 2',
};

export default function RootLayout({
  children,
}: {
  children: React.ReactNode;
}) {
  return (
    <html lang="en">
      <body className={inter.className}>
        <StyledComponentsRegistry>
          <AntApp>
            <NextAuthProvider>
              <Sidebar>{children}</Sidebar>
            </NextAuthProvider>
          </AntApp>
        </StyledComponentsRegistry>
      </body>
    </html>
  );
}
