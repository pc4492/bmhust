import { useEffect, useState } from 'react';

interface Props {
  initSeconds: number;
  secondsLeft: number;
  setSecondsLeft: (seconds: number) => void;
}

export default function CountdownTimer(props: Props) {
  const { secondsLeft, setSecondsLeft, initSeconds } = props;
  useEffect(() => {
    setSecondsLeft(initSeconds);
  }, []);

  useEffect(() => {
    if (secondsLeft <= 0) return;

    const timeout = setTimeout(() => {
      setSecondsLeft(secondsLeft - 1);
    }, 1000);

    return () => clearTimeout(timeout);
  }, [secondsLeft]);

  return (
    <div className="text-primary text-2xl font-semibold">
      <span>{Math.floor(secondsLeft / 60)} : </span>
      <span>{secondsLeft - Math.floor(secondsLeft / 60) * 60}</span>
    </div>
  );
}
