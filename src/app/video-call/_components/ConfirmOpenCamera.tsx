'use client';

import { pusherClient } from '@/lib/pusher';
import { getRoomName } from '@/src/utils/videocall';
import { App } from 'antd';
import Image from 'next/image';
import { useEffect } from 'react';

type ConfirmOpenCameraProps = {
  matchedUserId: string;
  currentUserId: string;
  setIsModalOpen: (isModalOpen: boolean) => void;
}

export default function ConfirmOpenCamera({ setIsModalOpen, matchedUserId, currentUserId }: ConfirmOpenCameraProps) {
  const { notification } = App.useApp();
  const roomName = getRoomName(matchedUserId, currentUserId);

  useEffect(() => {
    pusherClient.subscribe(roomName);
    const handleReceiveRequest = (data: any) => {
      console.log('handleReceiveRequest', data);
      if(data?.senderId === currentUserId) return;
      setIsModalOpen(true); 
    };

    pusherClient.bind('videocall:camera:open_request', handleReceiveRequest);

    return () => {
      pusherClient.unsubscribe(roomName);
      pusherClient.unbind('videocall:camera:open_request', handleReceiveRequest);
    };
  }, [currentUserId, setIsModalOpen]);

  const handleSendRequest = async () => {
    try {
      const response = await fetch(`/api/videocall/camera?receiverId=${matchedUserId}&roomName=${roomName}`);

      if (response.status === 200) {
        notification.success({ message: 'Đã gửi yêu cầu thành công' });
      } else {
        throw new Error();
      }
    } catch (error) {
      console.error(error);
      notification.error({ message: 'Gửi yêu cầu thất bại' });
    }
  };

  return (
    <div>
      <div
        onClick={handleSendRequest}
        className="rounded-3xl w-fit text-white bg-primary text-center py-1 px-4 flex items-center gap-3 hover:cursor-pointer"
      >
        <Image
          width={30}
          height={30}
          src={'/request_video_call.svg'}
          alt={'request_video_call'}
        />
        <p>Yêu cầu cuộc gọi video</p>
      </div>
    </div>
  );
}
