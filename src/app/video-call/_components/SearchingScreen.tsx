'use client';

import {
  CheckOutlined,
  EllipsisOutlined,
  PauseCircleFilled,
  PlayCircleFilled,
  UserOutlined,
} from '@ant-design/icons';
import { Avatar } from 'antd';
import { useSession } from 'next-auth/react';

interface User {
  name?: string | null;
  email?: string | null;
  image?: string | null;
}

interface SearchingScreenProps {
  isSearching: boolean;
  cancelSearching: () => void;
  handleSearching: () => void;
    currentUser: any;
}

const descriptions = [
  'Tâm sự cùng người lạ',
  'Tự do thể hiện cảm xúc',
  'Không lo sợ bị đánh giá',
];

export default function SearchingScreen(props: SearchingScreenProps) {
  // const session = useSession();
  const { isSearching, cancelSearching, handleSearching , currentUser} = props;

  return currentUser && (
    <div className="p-16 w-full">
      <div className="flex flex-col gap-6 items-center mt-32">
        <h2 className="text-primary text-4xl font-bold">Trò truyện ẩn danh</h2>
        <div className="flex w-full justify-around">
          <div className="flex flex-col items-center gap-2">
            <div className="p-1 border-4 border-stone-300 rounded-full">
              <Avatar size={150} src={currentUser?.image} />
            </div>
            <div className=" rounded-3xl w-fit text-white bg-primary text-center py-1 px-4">
              {currentUser?.name}
            </div>
          </div>
          {isSearching && (
            <div className="flex flex-col items-center gap-2">
              <div className="p-1 border-4 border-stone-300 rounded-full">
                <Avatar
                  size={150}
                  icon={<UserOutlined />}
                  style={{
                    color: 'gray',
                    backgroundColor: 'white',
                  }}
                />
              </div>
              <div className="rounded-3xl w-fit text-white bg-primary text-center py-1 px-4 p">
                <EllipsisOutlined />
              </div>
            </div>
          )}
        </div>

        <div className="flex flex-col items-center mb-4">
          <p className="text-xl font-bold text-primary mb-4">
            {isSearching
              ? 'Đang tìm kiếm...'
              : 'Kết nối tới những người bạn mới'}
          </p>

          <div className="flex flex-col items-start">
            {descriptions.map((item, index) => (
              <div key={`${index}-description`} className="flex gap-4 mt-2">
                <CheckOutlined
                  style={{
                    fontSize: 20,
                    color: 'pink',
                  }}
                />
                <p className="text-sm font-semibold">{item}</p>
              </div>
            ))}
          </div>
        </div>

        {isSearching ? (
          <div
            onClick={cancelSearching}
            className="bg-primary text-white flex gap-2 items-center px-4 py-2 rounded-3xl hover:cursor-pointer"
          >
            <PauseCircleFilled className="text-lg" />
            <span className="font-semibold">Dừng tìm kiếm</span>
          </div>
        ) : (
          <div
            onClick={handleSearching}
            className="bg-primary text-white flex gap-2 items-center px-4 py-2 rounded-3xl hover:cursor-pointer"
          >
            <PlayCircleFilled className="text-lg" />
            <span className="font-semibold">Bắt đầu</span>
          </div>
        )}
      </div>
    </div>
  );
}
