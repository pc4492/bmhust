'use client';

import { UserOutlined } from '@ant-design/icons';
import {
  ControlBar,
  DisconnectButton,
  LiveKitRoom,
  RoomAudioRenderer,
  useLiveKitRoom,
} from '@livekit/components-react';
import '@livekit/components-styles';
import { App, Avatar, Modal, Typography } from 'antd';
import cn from 'classnames';
import { useSession } from 'next-auth/react';
import { useEffect, useMemo, useState } from 'react';
import { BsFillTelephoneXFill } from 'react-icons/bs';
import ConfirmOpenCamera from './_components/ConfirmOpenCamera';
import CountdownTimer from './_components/CountdownTimer';
import MyVideoConference from './_components/MyVideoConference';
import SearchingScreen from './_components/SearchingScreen';

const wsUrl = process.env.NEXT_PUBLIC_LIVEKIT_URL;

export default function VideoCallPage() {
  const { notification } = App.useApp();

  // const session = useSession();

  const [searching, setSearching] = useState(false);
  const [isModalOpen, setIsModalOpen] = useState(false);
  const [token, setToken] = useState<string>('');
  const [uniqueRoomName, setUniqueRoomName] = useState<string>('');
  const [matchedUserId, setMatchedUserId] = useState<string>('');
  const [secondsLeft, setSecondsLeft] = useState<number>(0);
  const [videoState, setVideoState] = useState<{
    meUseVideo: boolean;
    otherUseVideo: boolean;
  }>({
    meUseVideo: false,
    otherUseVideo: false,
  });
  const [currentUser, setCurrentUser] = useState<any>({});

  // create new instance of Room (room in LiveKit)
  // to handle open camera - room.localParticipant.setCameraEnabled(true)
  const { room } = useLiveKitRoom({
    serverUrl: process.env.NEXT_PUBLIC_LIVEKIT_URL,
    token: token,
  });

  useEffect(() => {
    (async () => {
      try {
        const res = await fetch(`/api/users/me`, {method: 'GET'});
        if (res.status !== 200) {
          throw new Error('fetch user failed');
        }
        const user = await res.json();
        setCurrentUser(user);
      } catch (err) {
        console.error(err);
      }
    })();
  }, []);

  useEffect(() => {
    let intervalId: any;
    console.log(searching, token);
    // if (session.status !== 'authenticated') return;

    if (searching && token === '') {
      console.log('before interval');
      intervalId = setInterval(async () => {
        try {
          console.log('searching');
          const res = await fetch(`/api/livekit/searching?userEmail=${currentUser.email}`, { method: 'GET' });
          let data: any;
          switch(res.status) {
            case 200:
              data = await res.json();
              setUniqueRoomName(data.roomName);
              setMatchedUserId(data.matchedUserId);
              break;
            case 204:
              console.log('No matched user');
              return;
            case 404:
              console.log('Call not found');
            default:
              throw new Error('fetch matching user failed');
          }
          console.log('data', data);

          const getToken = await fetch(`/api/livekit?room=${data.roomName}&userEmail=${String(currentUser.email)}`);
          if (getToken.status !== 200) {
            throw new Error('fetch token failed');
          }
          const connectToken = (await getToken.json()).token;
          if (connectToken) {
            setToken(connectToken);
          } else {
            throw new Error('get token failed');
          }
        } catch (err) {
          console.error(err);
          notification.error({ message: 'Đã có lỗi xảy ra' });
        }
        setSearching(false);
      }, 1000);
    }

    return () => clearInterval(intervalId);
  }, [searching, currentUser]);

  const handleSearching = async () => {
    // if (session.status !== 'authenticated') return;
    try {
      // save request looking for call to database
      const looking = await fetch(`/api/livekit/searching`, {
        method: 'POST',
        body: JSON.stringify({
          userEmail: currentUser.email,
        }),
        headers: {
          'Content-Type': 'application/json',
        },
      });
      if (looking.status !== 201) {
        throw new Error('looking for call failed');
      }
      console.log('start searching');
      setSearching(true);
    } catch (error) {
      console.error(error);
      notification.error({ message: 'Đã có lỗi xảy ra khi tìm kiếm' });
    }
  };

  const cancelSearching = () => {
    // if (session.status !== 'authenticated') return;
    setSearching(false);
    setToken('');
    (async () => {
      try {
        await fetch(`/api/livekit/searching?userEmail=${currentUser.email}`, {
          method: 'DELETE',
        });
      } catch (err) {
        console.error(err);
        notification.error({ message: 'Đã có lỗi xảy ra khi hủy tìm kiếm' });
      }
    })();
  };

  const handleDisconnect = async () => {
    // if (session.status !== 'authenticated') return;
    try {
      await fetch(
        `/api/livekit?room=${uniqueRoomName}&userEmail=${currentUser.email}&time=${secondsLeft}`,
        {
          method: 'DELETE',
        }
      );
    } catch (err) {
      console.error(err);
      notification.error({ message: 'Đã có lỗi xảy ra khi ngat ket noi' });
    } finally {
      setToken('');
      setSearching(false);
    }
  };

  const isCalling = useMemo(() => {
    return token !== '' && searching === false;
  }, [token, searching]);

  const canRequestOpenCamera = useMemo(() => {
    if (videoState.otherUseVideo) return false;
    else return true;
  }, [videoState]);

  let observer: MutationObserver;
  let container: HTMLElement | null = null;
  if (token !== '' && !searching) {
    // custom fucking css cho mấy cái button
    let mic_button = document.querySelector(
      'button[data-lk-source="microphone"]'
    );
    let camera_button = document.querySelector(
      'button[data-lk-source="camera"]'
    );
    let cancer_button = document.querySelector(
      'button[class="lk-disconnect-button"]'
    );
    container = document.querySelector('div[class="lk-room-container"]');

    if (container && mic_button && camera_button && cancer_button) {
      observer = new MutationObserver((mutations) => {
        let change = mutations.filter(
          (mutation) =>
            mutation.type === 'childList' || mutation.type === 'attributes'
        );
        let isChanging = document
          .querySelector('div[data-lk-local-participant="true"]')
          ?.classList.contains('changing');
        if (change && container && !isChanging) {
          let meUseVideo =
            document
              .querySelector('div[data-lk-local-participant="true"]')
              ?.getAttribute('data-lk-video-muted') === 'false';
          let otherVideo = document.querySelector(
            'div[data-lk-local-participant="false"]'
          );
          let otherUseVideo =
            otherVideo !== null &&
            otherVideo.getAttribute('data-lk-video-muted') === 'false';
          setVideoState({
            meUseVideo: meUseVideo,
            otherUseVideo: otherUseVideo,
          });
        }
      });

      observer.observe(container, { childList: true, subtree: true });
      let otherScreen = container.querySelector(
        'div[data-lk-local-participant="false"]'
      );
      if (otherScreen) {
        observer.observe(otherScreen, { attributes: true, subtree: true });
      }

      container.setAttribute('style', `display: block;`);
      mic_button.setAttribute(
        'style',
        `background-color: transparent;color: #EC5F70;zoom: 1.5;`
      );
      mic_button.nextElementSibling?.remove();
      camera_button.setAttribute(
        'style',
        `background-color: transparent;color: #EC5F70;zoom: 1.5;`
      );
      camera_button.nextElementSibling?.remove();
      cancer_button.setAttribute(
        'style',
        `background-color: transparent;color: #EC5F70;zoom: 1.5;`
      );
    }
  }

  useEffect(() => {
    let meVideo = document.querySelector(
      'div[data-lk-local-participant="true"]'
    );
    let otherVideo = document.querySelector(
      'div[data-lk-local-participant="false"]'
    );

    meVideo?.classList.remove('show-video-bottom');
    otherVideo?.classList.remove('show-video-bottom');
    meVideo?.classList.remove('show-video-top');
    meVideo?.classList.add('changing');

    switch (
    (videoState.meUseVideo ? 1 : 0) | (videoState.otherUseVideo ? 2 : 0)
    ) {
      case 0:
        // console.log('meUseVideo: false, otherUseVideo: false');
        break;
      case 1:
        // console.log('meUseVideo: true, otherUseVideo: false');
        meVideo?.classList.add('show-video-bottom');
        break;
      case 2:
        // console.log('meUseVideo: false, otherUseVideo: true');
        if (otherVideo?.getAttribute('data-lk-video-muted') === 'false') {
          otherVideo?.classList.add('show-video-bottom');
        }
        break;
      case 3:
        // console.log('meUseVideo: true, otherUseVideo: true');
        meVideo?.classList.add('show-video-top');
        otherVideo?.classList.add('show-video-bottom');
        break;
    }

    setTimeout(() => {
      meVideo?.classList.remove('changing');
    }, 10);
  }, [videoState]);

  const handleOk = () => {
    // open camera
    room?.localParticipant.setCameraEnabled(true);
    setIsModalOpen(false);
  };

  const handleCancel = () => {
    setIsModalOpen(false);
  };

  return !currentUser ? (
    <div>Loading session...</div>
  ) : isCalling ? (
    <div>
      <div className="p-8 flex justify-between items-center">
        <h2 className="text-primary text-4xl font-bold">Trò truyện ẩn danh</h2>
        <CountdownTimer
          secondsLeft={secondsLeft}
          setSecondsLeft={setSecondsLeft}
          initSeconds={30 * 60}
        />
      </div>
      <LiveKitRoom
        video={false}
        audio={true}
        token={token}
        serverUrl={process.env.NEXT_PUBLIC_LIVEKIT_URL}
        connect={true}
        data-lk-theme="default"
        style={{ display: 'none', position: 'relative' }}
        onDisconnected={handleDisconnect}
        room={room}
      >
        <div
          className="flex flex-row items-center justify-center"
          style={{ height: 'calc(80vh - var(--lk-control-bar-height))' }}
        >
          {canRequestOpenCamera && (
            <div
              className={cn(
                'flex w-1/3 flex-wrap items-center justify-center gap-10 pl-4',
                !videoState.meUseVideo && !videoState.otherUseVideo && 'w-full'
              )}
            >
              <div className="p-1 border-4 border-stone-300 rounded-full">
                <Avatar
                  size={150}
                  icon={<UserOutlined />}
                  style={{
                    color: 'gray',
                    backgroundColor: 'white',
                  }}
                />
              </div>
              <ConfirmOpenCamera
                matchedUserId={matchedUserId}
                currentUserId={currentUser.id as string}
                setIsModalOpen={setIsModalOpen}
              />
            </div>
          )}

          <div
            className={cn('relative', canRequestOpenCamera ? 'w-2/3' : 'w-4/5')}
          >
            {/* Your custom component with basic video conferencing functionality. */}
            <MyVideoConference />
            {/* The RoomAudioRenderer takes care of room-wide audio for you. */}
            <RoomAudioRenderer />
          </div>
        </div>
        <div className="flex items-center justify-center mt-4">
          <ControlBar
            variation="minimal"
            controls={{
              screenShare: false,
              leave: false,
              chat: false,
            }}
          />
          <div className="py-3">
            <DisconnectButton
              style={{
                height: 36,
                background: 'black',
              }}
            >
              <BsFillTelephoneXFill />
            </DisconnectButton>
          </div>
        </div>
      </LiveKitRoom>
      <Modal open={isModalOpen} onOk={handleOk} onCancel={handleCancel}>
        <Typography.Title level={3}>
          Đối phương yêu cầu bạn mở camera.
        </Typography.Title>
      </Modal>
    </div>
  ) : (
    <SearchingScreen
      isSearching={searching}
      cancelSearching={cancelSearching}
      handleSearching={handleSearching}
      currentUser={currentUser}
    />
  );
}
