'use client';

import hometowns from '@/src/assets/hometown.json';
import { majorNames } from '@/src/assets/shared';
import { DeleteOutlined, SearchOutlined } from '@ant-design/icons';
import { Hobbies, User } from '@prisma/client';
import { useSearchParams } from 'next/navigation';
import {
  Button,
  Card,
  Checkbox,
  Col,
  Form,
  Row,
  Select,
  Skeleton,
  Typography,
} from 'antd';
import { useEffect, useMemo, useState } from 'react';
import CardItem from './_components/card-item';
import ErrorPane from './_components/error-pane';

export type Partner = User & {
  images: {
    id: string;
    url: string;
  }[];
  hobbies: Hobbies[];
};

export default function Home() {
  const [form] = Form.useForm();

  const [partners, setPartners] = useState<Partner[]>([]);
  const [hobbies, setHobbies] = useState<Hobbies[]>([]);
  const [isLoading, setIsLoading] = useState(false);
  const [error, setError] = useState<any>();

  // for select max tag render
  const [selectedMajor, setSelectedMajor] = useState<string[]>([]);
  const [selectedHobbies, setSelectedHobbies] = useState<string[]>([]);
  const [selectedHometown, setSelectedHometown] = useState<string[]>([]);

  const searchParams = useSearchParams();

  const listUserFake = [
      'tungnguyensnk@gmail.com',
      'bovainghiengnang2k1@gmail.com'
    ];
  // fake user
  let userIdFake: any = searchParams?.get('userId');

  if (userIdFake && Number(userIdFake) > listUserFake.length) {
    userIdFake = 0;
    document.cookie = `email=${listUserFake[userIdFake]}`;
  }

  if(!userIdFake && !document.cookie.includes('email')){
    userIdFake = 0;
    document.cookie = `email=${listUserFake[userIdFake]}`;
  }

  useEffect(() => {
    userIdFake && (document.cookie = `email=${listUserFake[userIdFake]}`);
    const fetchHobbies = async () => {
      try {
        const hobbies: Hobbies[] = await (await fetch('/api/hobbies')).json();

        setHobbies(hobbies);
      } catch (err) {
        console.error(err);
        // setError(err);
      }
    };

    const fetchPartners = async () => {
      try {
        const getPartners: Partner[] = await (await fetch('/api/users')).json();

        setPartners(getPartners);
      } catch (err) {
        console.error(err);
        // setError(err);
      }
    };
    // Get data from database
    setIsLoading(true);
    fetchHobbies();
    fetchPartners();
    setIsLoading(false);
  }, []);

  // 3 extra attributes selectedMajor, selectedHobbies, selectedHometown (for select max tag render "và x tiêu chí")
  // each time selectedMajor or selectedHobbies or selectedHometown -> this component rerender
  // -> CardItem also rerender -> solution: only rerender when partners update
  const renderListPartner = useMemo(() => {
    return (
      <CardItem
        items={partners.map((partner: Partner) => ({
          id: partner.id,
          address: partner.address!,
          hometown: partner.hometown!,
          major: partner.major!,
          name: partner.name,
          images: partner.images,
          hobbies: partner.hobbies.map((hobby) => hobby.name).join(', '),
          age:
            partner.grade !== null
              ? new Date().getFullYear() - (1955 - 18 + partner.grade)
              : 'Chưa có thông tin',
        }))}
      />
    );
  }, [partners]);

  const onFinish = async (values: any) => {
    try {
      // gender, hobbies, hometown, major is Array
      const { gender, hobbies, hometown, major, semester } = values;

      const queryUrl = `/api/users?${gender ? `gender=${gender}` : ''}${
        hometown ? `&hometown=${hometown}` : ''
      }${major ? `&major=${major}` : ''}${
        hobbies ? `&hobbies=${hobbies}` : ''
      }${semester ? `&semester=${semester}` : ''}`;
      const getPartners: Partner[] = await (await fetch(queryUrl)).json();

      // hold console log to check the search results at runtime
      console.log({
        getPartners,
      });

      setPartners(getPartners);
    } catch (err) {
      console.error(err);
      setError(err);
    }
  };

  const onReset = () => {
    form.resetFields();
  };

  if (error) {
    return <ErrorPane error={error} />;
  }

  return (
    <Card>
      <Skeleton loading={isLoading}>
        <Row>
          <Col lg={8} md={24} className="p-8">
            <Form
              layout="vertical"
              form={form}
              onFinish={onFinish}
              className="border rounded-lg"
            >
              <div className="bg-zinc-200 p-4 text-2xl text-center font-bold text-primary">
                Tiêu chí
              </div>
              <div className="p-4 space-y-4">
                <Form.Item
                  name="gender"
                  label={
                    <Typography.Title level={4}>Giới tính</Typography.Title>
                  }
                >
                  <Checkbox.Group
                    className="flex flex-col"
                    options={[
                      { label: 'Nam', value: 'male' },
                      { label: 'Nữ', value: 'female' },
                      { label: 'Khác', value: 'none' },
                    ]}
                  />
                </Form.Item>
                <Form.Item
                  name={'hobbies'}
                  label={
                    <Typography.Title level={4}>Sở thích</Typography.Title>
                  }
                >
                  <Select
                    allowClear
                    mode="multiple"
                    placeholder="Lựa chọn sở thích"
                    maxTagCount={2}
                    options={hobbies.map((hobby) => ({
                      label: hobby.name,
                      value: hobby.id,
                    }))}
                    onChange={(newValue: string[]) =>
                      setSelectedHobbies(newValue)
                    }
                    maxTagPlaceholder={`và ${
                      selectedHobbies.length - 2
                    } tiêu chí`}
                  />
                </Form.Item>
                <Form.Item
                  name={'hometown'}
                  label={
                    <Typography.Title level={4}>Quê quán</Typography.Title>
                  }
                >
                  <Select
                    allowClear
                    mode="multiple"
                    placeholder="Lựa chọn quê quán"
                    maxTagCount={2}
                    options={hometowns.map((item) => ({
                      label: item,
                      value: item,
                    }))}
                    onChange={(newValue: string[]) =>
                      setSelectedHometown(newValue)
                    }
                    maxTagPlaceholder={`và ${
                      selectedHometown.length - 2
                    } tiêu chí`}
                  />
                </Form.Item>
                <Form.Item
                  name={'major'}
                  label={
                    <Typography.Title level={4}>Chuyên ngành</Typography.Title>
                  }
                >
                  <Select
                    allowClear
                    mode="multiple"
                    placeholder="Lựa chọn chuyên ngành"
                    options={majorNames.map((i) => ({ label: i, value: i }))}
                    maxTagCount={2}
                    onChange={(newValue: string[]) =>
                      setSelectedMajor(newValue)
                    }
                    maxTagPlaceholder={`và ${
                      selectedMajor.length - 2
                    } tiêu chí`}
                  />
                </Form.Item>
                <div className="flex items-start gap-4 pt-4">
                  <Typography.Title level={4}>Khóa</Typography.Title>
                  <Form.Item name="semester" className="flex-1 !h-full !w-full">
                    <Select
                      allowClear
                      placeholder="Lựa chọn khóa"
                      options={[0, 1, 2, 3, 4, 5, 6, 7, 8].map((item) => {
                        const i = new Date().getFullYear() - 1955 - item;
                        return {
                          value: i,
                          label: i,
                        };
                      })}
                    />
                  </Form.Item>
                </div>
              </div>
              <div className="flex flex-row p-4 space-x-4 justify-around">
                <Button
                  htmlType="button"
                  onClick={onReset}
                  className="!border-primary !rounded-2xl"
                >
                  <div className="flex items-center gap-2 text-primary">
                    <DeleteOutlined />
                    <p>Đặt lại</p>
                  </div>
                </Button>
                <Button
                  type="primary"
                  htmlType="submit"
                  className="!rounded-2xl"
                >
                  <div className="flex items-center gap-2">
                    <SearchOutlined />
                    <p>Tìm kiếm</p>
                  </div>
                </Button>
              </div>
            </Form>
          </Col>
          {/* Display list partners */}
          <Col
            lg={16}
            md={24}
            span={14}
            className="!flex !items-center !justify-center w-full"
          >
            {renderListPartner}
          </Col>
        </Row>
      </Skeleton>
    </Card>
  );
}
