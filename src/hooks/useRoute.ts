import {
  HomeOutlined,
  UsergroupAddOutlined,
  VideoCameraOutlined,
  WechatOutlined
} from '@ant-design/icons';
import { usePathname } from 'next/navigation';
import { useMemo } from 'react';

const useRoute = () => {
  const pathname = usePathname();

  const routes = useMemo(
    () => [
      {
        label: 'Trang chủ',
        href: '/',
        icon: HomeOutlined,
        active: pathname === '/',
      },
      {
        label: 'Lời mời kết bạn',
        href: '/friends',
        icon: UsergroupAddOutlined,
        active: pathname === '/friends',
      },
      {
        label: 'Tin nhắn',
        href: '/chats',
        icon: WechatOutlined,
        active: pathname?.startsWith('/chats'),
      },
      {
        label: 'Gọi ẩn danh',
        href: '/video-call',
        icon: VideoCameraOutlined,
        active: pathname === '/video-call',
      },
    ],
    [pathname]
  );

  return routes;
};

export default useRoute;
