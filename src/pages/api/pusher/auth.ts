import { NextApiRequest, NextApiResponse } from 'next';
import { getServerSession } from 'next-auth';

import { pusherServer } from '@/lib/pusher';
import { authOptions } from '@/lib/auth';
import getCurrentUser from '@/src/actions/getCurrentUser';

export default async function handler(
	request: NextApiRequest,
	response: NextApiResponse
) {
	// const session = await getServerSession(request, response, authOptions);
	// console.log('session', session);
	const currentUser = await getCurrentUser();
	if (!currentUser) {
		return response.status(401);
	}

	const socketId = request.body.socket_id;
	const channel = request.body.channel_name;
	const data = {
		user_id: currentUser.id,
	};

	const authResponse = pusherServer.authorizeChannel(socketId, channel, data);
	return response.send(authResponse);
};
