export function getRoomName(n1: string, n2: string) {
  return `room_${[n1, n2].sort().join('_')}`;
}
