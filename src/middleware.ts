import { withAuth } from 'next-auth/middleware';

export default withAuth({
  secret: process.env.NEXTAUTH_SECRET,
  callbacks: {
    authorized: ({ token, req }) => {
      if (!token) {
        return false;
      }
      return true;
    },
  },
});

export const config = {
  matcher: [
    // '/:path*',
    // '/chats/:path*',
    // '/friends/:path*',
    // '/video-call/:path*',
  ]
};
