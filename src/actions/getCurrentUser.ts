import prisma from '@/lib/prisma';
import {getServerSession} from 'next-auth';
import {authOptions} from '@/lib/auth';

import {cookies} from 'next/headers';

const getCurrentUser = async () => {
    let email= cookies().get('email')?.value;
    try {
        let session = await getServerSession(authOptions);
        // if (!session?.user?.email) {
        //     return null;
        // }
        const currentUser = await prisma.user.findUnique({
            where: {
                email: session?.user?.email as string || email
            }
        });
        return currentUser;
    } catch (err) {
        return null;
    }
};

export default getCurrentUser;
