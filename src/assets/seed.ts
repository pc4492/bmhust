import { PrismaClient } from '@prisma/client';
import { faker } from '@faker-js/faker/locale/vi';
import { randomInt, randomUUID } from 'crypto';
import hometowns from './hometown.json';
import { hobbyNames, majorNames } from './shared';

const prisma = new PrismaClient();

const seedMessage = async () => {
  const users = await prisma.user.findMany({ take: 2 });
  if (users.length != 2) {
    throw Error('Must create 2 users first');
  }
  const user1 = users[0]!;
  const user2 = users[1]!;
  await prisma.messages.createMany({
    data: Array(10)
      .fill(0)
      .map((_) =>
        Math.random() < 0.5
          ? {
              senderId: user1.id,
              receiverId: user2.id,
              content: faker.lorem.sentence(),
              createdAt: new Date(new Date().setSeconds(randomInt(60))),
            }
          : {
              senderId: user2.id,
              receiverId: user1.id,
              content: faker.lorem.sentence(),
              createdAt: new Date(new Date().setSeconds(randomInt(60))),
            }
      ),
  });
};

const seedUser = async () => {
  const realUsers = [];

  // Create real user
  // await prisma.user.createMany({
  //   data: [
  //     {
  //       name: 'Kỳ Nguyễn Văn',
  //       email: 'luffy3042001@gmail.com',
  //       grade: 64,
  //       address: 'HUST, Ha Noi, Viet Nam',
  //       hometown: hometowns[randomInt(hometowns.length - 1)],
  //       major: majorNames[randomInt(majorNames.length - 1)],
  //       gender: 'male',
  //     },
  //     {
  //       name: 'Nguyễn Thị Thúy',
  //       email: 'bovainghiengnang2k1@gmail.com',
  //       grade: 64,
  //       address: 'HUST, Ha Noi, Viet Nam',
  //       hometown: hometowns[randomInt(hometowns.length - 1)],
  //       major: majorNames[randomInt(majorNames.length - 1)],
  //       gender: 'female',
  //     },
  //     {
  //       name: 'Lê Thị Hà',
  //       email: 'leha25052001@gmail.com',
  //       grade: 64,
  //       address: 'HUST, Ha Noi, Viet Nam',
  //       hometown: hometowns[randomInt(hometowns.length - 1)],
  //       major: majorNames[randomInt(majorNames.length - 1)],
  //       gender: 'male',
  //     }
  //   ]
  // });

  await prisma.hobbies.createMany({
    data: hobbyNames.map((name) => ({ name })),
  });

  await Promise.all(
    Array(randomInt(50,100))
      .fill(0)
      .map((_) => ({
        name: faker.person.fullName({ sex: 'female' }),
        email: 'z' + faker.internet.email(),
        grade: randomInt(60, 69),
        address: faker.location.street(),
        hometown: hometowns[randomInt(hometowns.length - 1)],
        major: majorNames[randomInt(majorNames.length - 1)],
        gender: 'female',
      }))
      .map(async (user) => {
        await prisma.user.create({
          data: {
            ...user,
            hobbies: {
              connect: hobbyNames
                .filter(() => randomInt(100) < 25)
                .map((hobby) => ({ name: hobby })),
            },
          },
        });
      })
  );
  await Promise.all(
    Array(randomInt(50,100))
      .fill(0)
      .map((_) => ({
        name: faker.person.fullName({ sex: 'male' }),
        email: 'z' + faker.internet.email(),
        grade: randomInt(60, 69),
        address: faker.location.street(),
        hometown: hometowns[randomInt(hometowns.length - 1)],
        major: majorNames[randomInt(majorNames.length - 1)],
        gender: 'male',
      }))
      .map(async (user) => {
        await prisma.user.create({
          data: {
            ...user,
            hobbies: {
              connect: hobbyNames
                .filter(() => randomInt(100) < 25)
                .map((hobby) => ({ name: hobby })),
            },
          },
        });
      })
  );

  let users = await prisma.user.findMany();
  const images = [
    'https://images.unsplash.com/photo-1494790108377-be9c29b29330?w=500&auto=format&fit=crop&q=60&ixlib=rb-4.0.3&ixid=M3wxMjA3fDB8MHxzZWFyY2h8M3x8YXZhdGFyfGVufDB8fDB8fHww',
    'https://images.unsplash.com/photo-1535713875002-d1d0cf377fde?w=500&auto=format&fit=crop&q=60&ixlib=rb-4.0.3&ixid=M3wxMjA3fDB8MHxzZWFyY2h8Mnx8YXZhdGFyfGVufDB8fDB8fHww',
    'https://images.unsplash.com/photo-1599566150163-29194dcaad36?w=500&auto=format&fit=crop&q=60&ixlib=rb-4.0.3&ixid=M3wxMjA3fDB8MHxzZWFyY2h8NHx8YXZhdGFyfGVufDB8fDB8fHww',
    'https://images.unsplash.com/photo-1633332755192-727a05c4013d?w=500&auto=format&fit=crop&q=60&ixlib=rb-4.0.3&ixid=M3wxMjA3fDB8MHxzZWFyY2h8OHx8YXZhdGFyfGVufDB8fDB8fHww',
    'https://images.unsplash.com/photo-1438761681033-6461ffad8d80?w=500&auto=format&fit=crop&q=60&ixlib=rb-4.0.3&ixid=M3wxMjA3fDB8MHxzZWFyY2h8MTF8fGF2YXRhcnxlbnwwfHwwfHx8MA%3D%3D',
    'https://images.unsplash.com/photo-1654110455429-cf322b40a906?w=500&auto=format&fit=crop&q=60&ixlib=rb-4.0.3&ixid=M3wxMjA3fDB8MHxzZWFyY2h8ODR8fGF2YXRhcnxlbnwwfHwwfHx8Mg%3D%3D',
    'https://images.unsplash.com/photo-1649123245135-4db6ead931b5?w=500&auto=format&fit=crop&q=60&ixlib=rb-4.0.3&ixid=M3wxMjA3fDB8MHxzZWFyY2h8ODN8fGF2YXRhcnxlbnwwfHwwfHx8Mg%3D%3D',
    'https://images.unsplash.com/photo-1508214751196-bcfd4ca60f91?w=500&auto=format&fit=crop&q=60&ixlib=rb-4.0.3&ixid=M3wxMjA3fDB8MHxzZWFyY2h8OTF8fGF2YXRhcnxlbnwwfHwwfHx8Mg%3D%3D',
    'https://images.unsplash.com/photo-1636041248714-bdf6287cc2ea?w=500&auto=format&fit=crop&q=60&ixlib=rb-4.0.3&ixid=M3wxMjA3fDB8MHxzZWFyY2h8MTA5fHxhdmF0YXJ8ZW58MHx8MHx8fDI%3D',
    'https://images.unsplash.com/photo-1636041293723-abceb81bffbe?w=500&auto=format&fit=crop&q=60&ixlib=rb-4.0.3&ixid=M3wxMjA3fDB8MHxzZWFyY2h8MTEwfHxhdmF0YXJ8ZW58MHx8MHx8fDI%3D',
    'https://images.unsplash.com/photo-1504593811423-6dd665756598?w=500&auto=format&fit=crop&q=60&ixlib=rb-4.0.3&ixid=M3wxMjA3fDB8MHxzZWFyY2h8MTN8fHBlcnNvbnxlbnwwfHwwfHx8Mg%3D%3D',
    'https://images.unsplash.com/photo-1542206395-9feb3edaa68d?w=500&auto=format&fit=crop&q=60&ixlib=rb-4.0.3&ixid=M3wxMjA3fDB8MHxzZWFyY2h8MjR8fHBlcnNvbnxlbnwwfHwwfHx8Mg%3D%3D',
    'https://images.unsplash.com/photo-1573496359142-b8d87734a5a2?w=500&auto=format&fit=crop&q=60&ixlib=rb-4.0.3&ixid=M3wxMjA3fDB8MHxzZWFyY2h8MzZ8fHBlcnNvbnxlbnwwfHwwfHx8Mg%3D%3D'
  ];

  await prisma.image.createMany({
    data: [
      ...users,
      ...users.filter(() => randomInt(10) < 5),
      ...users.filter(() => randomInt(10) < 5),
    ].map((user) => ({
      userId: user.id,
      url: images[randomInt(images.length - 1)],
    })),
  });
};

// seedMessage();

seedUser();
