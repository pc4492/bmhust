export const hobbyNames = [
  'Đọc sách',
  'Nghe nhạc',
  'Xem phim',
  'Thể thao',
  'Du lịch',
  'Nấu ăn',
  'Chơi nhạc',
  'Chơi game',
  'Thiền',
  'Chăm sóc thú cưng',
];

export const majorNames = [
  'Global ICT',
  'HEDSPI',
  'Khoa học máy tính',
  'Kỹ thuật máy tính',
  'Diến tử viễn thông',
  'Kỹ Thuật y sinh',
  'Cơ điện tử',
  'Kỹ thuật ô tô',
  'Kỹ thuật hàng không',
  'Khoa học máy tính - ĐH Troy (Hoa Kỳ)',
  'Toán - Tin',
  'Vật lý kỹ thuật',
  'Cơ khí hàng không'
];
